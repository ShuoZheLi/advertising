package com.example.advertising;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.le.AdvertiseData;
import android.bluetooth.le.AdvertisingSet;
import android.bluetooth.le.AdvertisingSetCallback;
import android.bluetooth.le.AdvertisingSetParameters;
import android.bluetooth.le.BluetoothLeAdvertiser;
import android.bluetooth.le.BluetoothLeScanner;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.ParcelUuid;
import android.util.Log;
import android.widget.Toast;

import java.nio.charset.Charset;
import java.util.List;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {
    String msg = "Android : ";
    private BluetoothAdapter mBluetoothAdapter = BluetoothAdapter
            .getDefaultAdapter();
    private BluetoothDevice targetDevice;
    private BluetoothLeScanner bluetoothLeScanner;
    private List<BluetoothGattService> mServiceList;
    private UUID targetUUID = UUID.fromString("8ce255c0-200a-11e0-ac64-0800200c9a66");
    ParcelUuid pUuid = new ParcelUuid( targetUUID );
    private BluetoothLeAdvertiser advertiser =
            BluetoothAdapter.getDefaultAdapter().getBluetoothLeAdvertiser();

    private AdvertisingSet currentAdvertisingSet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(msg, "The onCreate() event");
        if(mBluetoothAdapter == null){
            Toast.makeText(this, "不支持蓝牙", Toast.LENGTH_SHORT).show();
        }else{
            enableBT();
        }

    }

    // method enable Bluetooth
    protected void enableBT(){
        // case: bluetooth already enabled
        if(mBluetoothAdapter.isEnabled()){
            Log.d(msg, "蓝牙已经提前开启");
            Toast.makeText(this, "蓝牙已经提前开启", Toast.LENGTH_SHORT).show();
        }else{
            // case: bluetooth not enabled yet
            Toast.makeText(this, "正在开启蓝牙", Toast.LENGTH_SHORT).show();
            Intent enableBtIntent = new Intent(
                    BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, 1);
        }

        // start advertising
        enableAdvert();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    protected void enableAdvert(){
        AdvertisingSetParameters parameters = (new AdvertisingSetParameters.Builder())
                .setLegacyMode(true) // True by default, but set here as a reminder.
                .setConnectable(true)
                .setScannable(true)
                .setInterval(AdvertisingSetParameters.INTERVAL_HIGH)
                .setTxPowerLevel(AdvertisingSetParameters.TX_POWER_MEDIUM)
                .build();

        //Integer x = 1;
        byte[] bytes = {69};
//        AdvertiseData data = (new AdvertiseData.Builder())
//                .setIncludeDeviceName(true)
//                .addServiceUuid( pUuid )
//                .addServiceData( pUuid, bytes )
//                //.addServiceData( pUuid, x.getBytes( Charset.forName( "UTF-8" ) ) )
//                .build();
        AdvertiseData data = (new AdvertiseData.Builder()).setIncludeDeviceName(true).build();

        AdvertisingSetCallback callback = new AdvertisingSetCallback() {
            @Override
            public void onAdvertisingSetStarted(AdvertisingSet advertisingSet, int txPower, int status) {
                Log.d(msg, "#####onAdvertisingSetStarted(): txPower:" + txPower + " , status: "
                        + status);
                currentAdvertisingSet = advertisingSet;
                // After onAdvertisingSetStarted callback is called, you can modify the
                // advertising data and scan response data:
                advertisingSet.setAdvertisingData(new AdvertiseData.Builder().
                        setIncludeDeviceName(true).setIncludeTxPowerLevel(true).build());
            }

            @Override
            public void onAdvertisingDataSet(AdvertisingSet advertisingSet, int status) {
                Log.d(msg, "#####onAdvertisingDataSet() :status:" + status);
                byte[] bytes = {69};
                // Wait for onAdvertisingDataSet callback...
                advertisingSet.setScanResponseData(
                        new AdvertiseData.Builder().addServiceUuid(new ParcelUuid(targetUUID))
                        //.addServiceData( pUuid, "".getBytes( Charset.forName( "UTF-8" ) ) )
                        .addServiceData( pUuid, bytes )
                        .build());
            }

            @Override
            public void onScanResponseDataSet(AdvertisingSet advertisingSet, int status) {
                Log.i(msg, "onScanResponseDataSet(): status:" + status);
            }

            @Override
            public void onAdvertisingSetStopped(AdvertisingSet advertisingSet) {
                Log.i(msg, "onAdvertisingSetStopped():");
            }
        };
        Log.i(msg, "##############################");
        Log.i(msg, "##############################");
        advertiser.startAdvertisingSet(parameters, data, null, null, null, callback);






        // Wait for onScanResponseDataSet callback...

        // When done with the advertising:
        // advertiser.stopAdvertisingSet(callback);
    }
}
